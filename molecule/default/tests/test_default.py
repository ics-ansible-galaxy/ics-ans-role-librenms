import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_librenms_containers(host):
    with host.sudo():
        cmd = host.command('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.split('\n')[1:]])
    assert names == ['librenms', 'librenms_mariadb', 'traefik_proxy']


def test_librenms_index(host):
    # This tests that traefik forwards traffic to the librenms web server
    # and that we can access the librenms login page
    cmd = host.command('curl -H Host:ics-ans-role-librenms-default -k -L https://localhost')
    assert '<title>LibreNMS</title>' in cmd.stdout


def test_librenms_ldap(host):
    cmd = host.command("sudo docker exec librenms sh -c 'cd /opt/librenms && php scripts/auth_test.php -l'")
    assert 'Authentication Method: ldap' in cmd.stdout
