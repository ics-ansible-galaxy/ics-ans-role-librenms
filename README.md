ics-ans-role-librenms
===================

Ansible role to install LibreNMS network monitoring system.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Layout
------

                          +-----------------------+
                          |                       |   
                          | ics-ans-role-librenms |
                          |                       |                
                          +-----------------------+
                        
                             /              \         
                            /                \
                            
             +------------+                     +---------------------+
             |            |                     |                     |   
             |  librenms  |                     |  librenms_mariadb   |
             |            |                     |                     |                
             +------------+                     +---------------------+
             
                                              
The librenms role is composed by two docker images,
[Librenms](https://https://gitlab.esss.lu.se/) and the official
[MariaDB](https://hub.docker.com/_/mariadb/).

Enviroment Variables for Container Creation
-------------------------------------------

```yaml
# librenms
env:
  POLLERS: 16
  SNMP_SCAN_ENABLE: true

# librenms_mariadb
env:
  MYSQL_ROOT_PASSWORD: "{{ librenms_db_root_password }}"
  MYSQL_DATABASE: "{{ librenms_db }}"
  MYSQL_USER: "{{ librenms_db_user }}"
  MYSQL_PASSWORD: "{{ librenms_db_password }}"

```

Role Variables
--------------

```yaml
librenms_docker_image: registry.esss.lu.se/ics-docker/librenms/nfsen
librenms_version: latest
librenms_network: librenms-network
librenms_hostname: "{{ ansible_fqdn }}"
librenms_db_root_password: "{{ vault_librenms_db_root_password }}"
librenms_db: librenms
librenms_db_user: librenms
librenms_db_password: "{{ vault_librenms_db_password }}"
librenms_admin_password: "{{ vault_librenms_admin_password }}"
librenms_auth_mechanism: ldap
librenms_admin_email: test@example.com
librenms_uid: 999

# Paths
librenms_dir: /opt/librenms
librenms_logs: "{{ librenms_dir }}/logs"
librenms_rrd: "{{ librenms_dir }}/rrd"
librenms_config: "{{ librenms_dir }}/config.php"
librenms_db_dir: "{{ librenms_dir }}/db"
librenms_backup: "{{ librenms_dir }}/backup"
librenms_nfsen_data_dir: "{{ librenms_dir }}/data/nfsen/profiles-data/live"
librenms_nfsen_stat_dir: "{{ librenms_dir }}/data/nfsen/profiles-stat/live"
librenms_nfsen_config: "{{ librenms_dir }}/nfsen.conf"
librenms_mariadb_config: "{{ librenms_dir }}/librenms.cnf"
librenms_ldap_config: "{{ librenms_dir }}/ldap.conf"

# LDAP
librenms_ldap_version: 3
librenms_ldap_server: ldaps://172.16.6.11
librenms_ldap_port: 636
librenms_ldap_suffix: ",ou=Users,dc=esss,dc=lu,dc=se"
librenms_ldap_groups_admin: libre-admin
librenms_ldap_groups_admin_level: 10
librenms_ldap_groups_user: libre-user
librenms_ldap_groups_user_level: 1
librenms_ldap_groupbase: "ou=Posix,ou=Groups,dc=esss,dc=lu,dc=se"

# SNMP Community
librennms_snmp_community:
  - icslabcom
  - icscsitn
  - public

# Network
librenms_network_discovery: []

# 1 Minute Polling
librenms_rrd_step: 60
librenms_rrd_heartbeat: 120
```

Ansible-vault
-------------

Is used for encryption of passwords. In [AWX](https://torn.tn.esss.lu.se/) use the credential called ```ansible-vault```. 
To run a local instance, create a ```.vault_pass``` file in the role directory with the ansible vault password, which can be found on the password page. 

Backup
------

[Elkarbackup](http://172.16.107.20/elkarbackup/login) is taking a backup of ```/opt/librenms/``` on the virtual machine (```db/``` and ```logs/``` are excluded).

The mysql dump script is located at ```/usr/local/bin/mariadb-backup.sh```


Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-librenms
```

License
-------

BSD 2-clause
